/* global MemberStack */
/* eslint no-undef: "error" */

var baseURL;

async function fileBrowser(iframe) {
    MemberStack.onReady.then(function(member) {
        if (member.loggedIn) {
            const token = MemberStack.getToken();

            if (typeof iframe.src === 'undefined' || iframe.src === '') {
                iframe.style.display = 'block';
                iframe.src = `${baseURL}/files/?auth=${token}`;
            }
        }
    });
}

window.onload = function() {
    const me = document.querySelector('script[data-id="fileBrowser"]');
    if (!me) {
        console.error("script attributes didn't specify");
    }

    const url = new URL(me.src);
    baseURL = `${url.protocol}//${url.host}`;

    const btnSelector = me.getAttribute('data-btn');
    if (!btnSelector) {
        console.error("didn't specify an attribute: data-btn ");
    }

    const iframeSelector = me.getAttribute('data-iframe');
    if (!iframeSelector) {
        console.error("didn't specify an attribute: data-iframe ");
    }

    const btn = document.querySelector(btnSelector);
    if (!btn) {
        console.error("didn't find an element with the selector: " + btnSelector);
    }

    const iframe = document.querySelector(iframeSelector);
    if (!iframe) {
        console.error("didn't find an element with the selector: " + iframeSelector);
    }

    btn.addEventListener('click', function() {
        fileBrowser(iframe);
    });
};
