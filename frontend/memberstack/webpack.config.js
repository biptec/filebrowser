const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const isDevelopment = process.env.NODE_ENV === 'development';

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry: ['./index.js'],
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'memberstack.js',
        publicPath: '/',
    },
    devServer: {
        host: '0.0.0.0',
        port: 8081,
        contentBase: path.resolve(__dirname, 'public'),
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [new MiniCssExtractPlugin()],
    module: {
        rules: [
            {
                test: /(\.jsx|\.js)$/,
                enforce: 'pre',
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'eslint-loader',
                        options: {
                            failOnWarning: false,
                            failOnError: true,
                            quiet: true,
                        },
                    },
                ],
            },
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(svg|gif|png|eot|woff|ttf)$/,
                use: ['url-loader'],
            },
        ],
    },
    optimization: {
        minimize: true,
        minimizer: [new OptimizeCssAssetsPlugin()],
    },
};
