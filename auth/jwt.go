package auth

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/filebrowser/filebrowser/v2/settings"
	"github.com/filebrowser/filebrowser/v2/users"
)

// MethodJWTAuth is used to identify jwt auth.
const MethodJWTAuth settings.AuthMethod = "jwt"

// JWTAuth is a json implementation of an Auther.
type JWTAuth struct {
	PublicKey string `json:"key" yaml:"key"`
}

// Auth authenticates the user via a jwt tokenString in header.
func (a JWTAuth) Auth(r *http.Request, stoUser *users.Storage, stoSettings *settings.Storage, root string) (*users.User, error) {
	claims, err := a.parseToken(r)
	if err != nil {
		log.Printf("parse token: %s", err)
		return nil, os.ErrPermission
	}

	username, ok := claims["email"].(string)
	if !ok || username == "" {
		log.Printf("did not specify email in the token")
		return nil, os.ErrPermission
	}

	// Try to get an existing user
	if user, err := stoUser.Get(root, username); err == nil {
		return user, nil
	}

	// Create a new user
	user := &users.User{
		Username: username,
		Password: randStringRunes(32),
	}

	settings, err := stoSettings.Get()
	if err != nil {
		return nil, err
	}
	settings.Defaults.Apply(user)

	userHome, err := settings.MakeUserDir(username, "", root)
	if err != nil {
		log.Printf("create user: failed to mkdir user home dir: [%s]", userHome)
		return nil, os.ErrPermission
	}
	user.Scope = userHome
	log.Printf("user: %s, home dir: [%s].", username, userHome)

	if err := stoUser.Save(user); err != nil {
		return nil, err
	}

	return user, nil
}

// LoginPage tells that json auth doesn't require a login page.
func (a JWTAuth) LoginPage() bool {
	return false
}

// Init loads publid key.
func (a *JWTAuth) Init(key string) error {
	publicKey, err := ioutil.ReadFile(key)
	if err != nil {
		return err
	}

	a.PublicKey = string(publicKey)

	return nil
}

func (a JWTAuth) parseToken(r *http.Request) (jwt.MapClaims, error) {
	publicRSA, err := jwt.ParseRSAPublicKeyFromPEM([]byte(a.PublicKey))
	if err != nil {
		return nil, err
	}

	token, err := request.ParseFromRequest(r, &extractor{}, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodRSA)
		if !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return publicRSA, nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !token.Valid || !ok {
		return nil, errors.New("invalid token")
	}

	if err := claims.Valid(); err != nil {
		return nil, err
	}

	return claims, nil
}

type extractor []string

func (e extractor) ExtractToken(r *http.Request) (string, error) {
	token, _ := request.HeaderExtractor{"Authorization"}.ExtractToken(r)

	if token != "" && strings.Count(token, ".") == 2 {
		return token, nil
	}

	return "", request.ErrNoTokenInRequest
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func init() {
	rand.Seed(time.Now().UnixNano())
}
